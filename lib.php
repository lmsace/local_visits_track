<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Lib file for component. Add the javascript for each to find the user timespent.
 *
 * @package   local_visits_track
 * @copyright Moodle Dev
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die('No Direct Access');

/**
 * Extend the settings navigation and add the timetracker js to page.
 *
 * @param settings_navigation $nav
 * @param context $context
 * @return void
 */

function local_visits_track_extend_settings_navigation(settings_navigation $nav, context $context) {
    global $PAGE, $USER;

    $params = [
        'courseid'  => $PAGE->course->id,
        'contextid' => $PAGE->context->id,
        'cmid'      => isset($PAGE->cm->id) ? $PAGE->cm->id : null
    ];
    if (!is_siteadmin()) {
        $PAGE->requires->js_call_amd('local_visits_track/timetracker', 'init', ['pageData' => $params, 'userid' => $USER->id ]);
    }
}
