<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local visits Tracker reports base.
 *
 * @package   local_visits_tracker
 * @copyright Moodle Dev
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_visits_track;

defined('MOODLE_INTERNAL') || die('No Direct Access');

use external_function_parameters;
use external_single_structure;
use external_value;

/**
 * External sevice function to update the user time spent.
 */
class external extends \external_api {

    /**
     * Needed Parameters to update the user time.
     *
     * @return external_function_parameters
     */
    public static function updatetime_parameters() {
        return  new external_function_parameters([

            'pagedata' => new external_single_structure(
                array(
                    'timespent' => new external_value(PARAM_FLOAT, 'Spent time by user'),
                    'contextid' => new external_value(PARAM_INT, 'Page context id'),
                    'courseid' => new external_value(PARAM_FLOAT, 'course id of the current page'),
                    'cmid' => new external_value(PARAM_INT, 'Spent time by user', VALUE_OPTIONAL),
                )
            )
        ]);
    }

    /**
     * Update the user time spent on the Portal for each 60 seconds.
     *
     * @param array $pagedata
     * @return bool
     */
    public static function updatetime(array $pagedata) : bool {
        global $USER, $SESSION;
        global $DB;

        if (!empty($pagedata) && !is_siteadmin() && isset($pagedata['courseid']) && isset($pagedata['contextid'])) {

            $context = \context_course::instance($pagedata['courseid']);
            if (is_guest($context, $USER) && !is_enrolled($context)) {
                return false;
            }

            $userid   = $USER->id;
            $params = [
                'courseid'  => $pagedata['courseid'],
                'contextid' => $pagedata['contextid'],
                'cmid'      => $pagedata['cmid'],
                'userid'    => $userid,
                'sessionid' => session_id()
            ];

            if ($record = $DB->get_record('local_visits_track', $params)) {
                $record->timespent += $pagedata['timespent'];
                return ($DB->update_record('local_visits_track', $record)) ? true : false;
            } else {
                $params['timecreated'] = time();
                $params['timespent'] = $pagedata['timespent'];
                return ($DB->insert_record('local_visits_track', $params)) ? true : false;
            }
        }

        return false;
    }

    /**
     * Update time returns.
     *
     * @return void
     */
    public static function updatetime_returns() {
        return new external_value(PARAM_BOOL, 'Result of time update');
    }
}
