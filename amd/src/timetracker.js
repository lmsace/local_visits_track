define(['jquery', 'core/ajax'], function($, AJAX) {


    /* global TimeSpentPageData */

    let startStopTimes = [];

    var THIS;

    let updateInterval = 30000 // Update time in miliseconds 30 seconds.

    var TimeSpentInSeconds = 0;

    var inActiveTime = 120; // 60 seconds.

    var progressInterval = 1000 // 1 second.

    var TimeSpent = function(pageData, userid) {
        this.pageData = pageData;
        this.userid = userid;
        this.startTimer();
        this.setUpEvents();
        THIS = this;
    };

    TimeSpent.prototype.pageData = {};

    TimeSpent.prototype.userid = null;

    TimeSpent.prototype.idealCounter = 0;

    TimeSpent.prototype.pageData = {};

    TimeSpent.prototype.currentTime = function() {
        return new Date();
    };

    TimeSpent.prototype.startTimer = function() {
        /* if (!this.userInViewPort()) {
            return 0;
        } */
        if (startStopTimes.length && startStopTimes[startStopTimes.length - 1].stopTime === null) {
            startStopTimes[startStopTimes.length - 1].stopTime = this.currentTime();
        }

        startStopTimes.push({
            startTime: this.currentTime(),
            stopTime: null
        });
    };

    TimeSpent.prototype.stopTimer = function() {
        if (!startStopTimes.length) {
            console.log('Need to start the time first before stop it');
        }
        startStopTimes[startStopTimes.length - 1].stopTime = this.currentTime();
    };

    

    TimeSpent.prototype.getTimeSpentInSeconds = function() {
        if (startStopTimes.length != '') {
            startStopTimes.forEach((time, key) => {
                if (time.stopTime === null) {
                    startStopTimes[key].stopTime = THIS.currentTime();
                }
                var diff = (time.stopTime - time.startTime);
                TimeSpentInSeconds += diff;
                TimeSpentInSeconds = parseInt(TimeSpentInSeconds / 1000);
            })
        }
    };

    TimeSpent.prototype.updateUserTime = (reStart=false) => {

        THIS.getTimeSpentInSeconds();
        if (reStart) {
            THIS.startTimer();
        }

        if (TimeSpentInSeconds == 0) {
            return;
        }
        THIS.pageData['timespent'] = TimeSpentInSeconds;
        var params = { 'pagedata': THIS.pageData };
        var promise = AJAX.call([
            { methodname: 'local_visits_track_updatetime', args: params }
        ]);
        promise[0].done((response) => {});
        
        TimeSpentInSeconds = 0;
        startStopTimes = [];
    };

    TimeSpent.prototype.setUpEvents = function() {

        setInterval(function() {
            THIS.updateUserTime();
        }, updateInterval);

        setInterval(() => THIS.TimeSpentProgress(), progressInterval);

        if (document.addEventListener) {
            document.addEventListener('visibilitychange', () => THIS.checkVisibility() );
            document.addEventListener("mousemove", () => THIS.clearIdealMode() );
	        document.addEventListener("keypress", () => THIS.clearIdealMode() );
	        document.addEventListener("scroll", () => THIS.clearIdealMode() );
            window.addEventListener('beforeunload', () => THIS.checkVisibility(false));
        } else if (document.attachEvent) {
            document.attachEvent('visibilitychange', () => THIS.checkVisibility());
            document.attachEvent('beforeunload', () => THIS.checkVisibility(false));
        }
    };

    TimeSpent.prototype.clearIdealMode = () => {
        THIS.idealCounter = 0;
        if (!startStopTimes.length || startStopTimes[startStopTimes.length - 1].stopTime !== null) {
            THIS.startTimer();
        }
    };

    TimeSpent.prototype.isTimerRuninng = () => { 
        return (startStopTimes.length && startStopTimes[startStopTimes.length - 1].stopTime === null) ? true : false;
    };

    TimeSpent.prototype.TimeSpentProgress = () => {

        
            var status = THIS.mediaProgress();
            if (status && !document.hidden) {
                THIS.clearIdealMode();
            }
        // }

        if (!document.hidden && THIS.idealCounter >= inActiveTime && THIS.isTimerRuninng()) {
            THIS.updateUserTime(false);
        }
        THIS.idealCounter++;
    }

    TimeSpent.prototype.mediaProgress = () => {
        var media = [];
        var status = false;
        var internal = document.querySelectorAll('audio,video');
        var frames = document.querySelectorAll('iframe');
        if (frames.length) {
            frames.forEach(function(frame) {
                try {
                    var elements = frame.contentWindow.document.querySelectorAll('audio,video');
                    if (elements.length) {
                        elements.forEach(function(element) {
                            media.push(element);
                        });
                    }
                } catch (e) {}
            });
        }
        if (internal.length) {
            internal.forEach(function(element) {
                media.push(element);
            });
        }
    
        if (media.length) {
            media.forEach(function(element) {
                if (!element.paused) {
                    status = true;
                }
            });
        }
        return status;
    }


    TimeSpent.prototype.checkVisibility = function() {
        if (document.visibilityState == 'visible') {
            THIS.updateUserTime(true);
        } else {
            THIS.updateUserTime(false);
        }
    };

    return {
        init: function(pageData, userid) {
            new TimeSpent(pageData, userid);
        }
    };
})